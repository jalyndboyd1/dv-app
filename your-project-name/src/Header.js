import React from "react";
import Avatar from "@material-ui/core/Avatar";
import picture from "./prof-pic.jpg";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import TwitterIcon from "@material-ui/icons/Twitter";
import { IconButton } from "@material-ui/core";
import "./Header.css";
import { Link } from "react-router-dom";

function Header({ avatar, name, location }) {
  return (
    <div className="header">
      <div className="header-container">
        <div className="header-left">
          <Link to="settings">
            <IconButton>
              <Avatar src={picture} alt="Jalyn" />
            </IconButton>
          </Link>

          <h2 className="header-name">Jalyn</h2>
          <p>
            <b>Indianapolis, IN</b>
          </p>
        </div>
        <div className="header-center">{/* DV APP Logo */}</div>
        <div className="header-right">
          <IconButton>
            <FacebookIcon />
          </IconButton>
          <IconButton>
            <InstagramIcon />
          </IconButton>
          <IconButton>
            <TwitterIcon />
          </IconButton>
          <IconButton>
            <LinkedInIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
}

export default Header;
