import "./App.css";
import Footer from "./Footer";
import Header from "./Header";
import Quotes from "./Quotes";
// import Resources from "./Resources";
import React from "react";
import RouteButtons from "./RouteButtons";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="app">
      <Router>
        <div className="app-container">
          <Header />
          {/* Main Page */}
          <RouteButtons />
          <center className="app-quotes">
            <Quotes />
          </center>
          {/* Roots Sidebar */}
          {/* Resoures */}
          {/* Posts */}
          <Footer />
        </div>
      </Router>
    </div>
  );
}

export default App;
