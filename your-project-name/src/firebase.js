import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDOGEKJxiuAG0BZXnEcz2Q13tbUDXo-dls",
  authDomain: "dv-app-17b9c.firebaseapp.com",
  projectId: "dv-app-17b9c",
  storageBucket: "dv-app-17b9c.appspot.com",
  messagingSenderId: "1066935504074",
  appId: "1:1066935504074:web:efda7688c68aa19d3db833",
  measurementId: "G-2185YTMBPL",
};

const db = fireBaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { db, auth, storage, firebase };
