import React, { Component } from "react";
import "./Quotes.css";
const QUOTES = [
  "Leaving is the hardest part. Congratulations on taking that first step.",
  "The average victim goes back to their abuser seven times before leaving.",
  "Not all abuse can be seen by others.",
  "Be Empowered",
];

class Quotes extends Component {
  state = {
    quoteIndex: 0,
    fadeIn: true,
  };

  componentDidMount() {
    this.timeOut = setTimeout(() => this.setState({ fadeIn: false }), 2000);
    this.animateTitles();
  }

  animateTitles = () => {
    this.quoteInterval = setInterval(() => {
      const quoteIndex = (this.state.quoteIndex + 1) % QUOTES.length;
      this.setState({ quoteIndex, fadeIn: true });
      setTimeout(() => this.setState({ fadeIn: false }), 2000);
    }, 4000);
  };

  componentWillUnmount() {
    clearInterval(this.quoteInterval);
    clearInterval(this.timeOut);
  }
  render() {
    const { fadeIn, quoteIndex } = this.state;
    const quote = QUOTES[quoteIndex];
    return (
      <p className={fadeIn ? "title-fade-in" : "title-fade-out"}>{quote}</p>
    );
  }
}

export default Quotes;
