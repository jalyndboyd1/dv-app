import { Button } from "@material-ui/core";
import React from "react";
import "./Footer.css";

function Footer() {
  return (
    <div className="footer">
      <div className="footer-container">
        <Button>Log in</Button>
        <Button>Log Out</Button>
        <Button>Sign Up</Button>
        <p>Made for Survivors by Survivors</p>
      </div>
    </div>
  );
}

export default Footer;
