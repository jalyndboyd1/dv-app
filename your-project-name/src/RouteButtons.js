import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import PeopleIcon from "@material-ui/icons/People";
import { IconButton } from "@material-ui/core";
import "./RouteButtons.css";
import { Link } from "react-router-dom";

function RouteButtons() {
  return (
    <div className="routebuttons">
      <div className="route-container">
        <Link to="/">
          <IconButton>
            <HomeIcon fontSize="large" />
          </IconButton>
        </Link>
        <Link to="/resources">
          <IconButton>
            <FavoriteBorderIcon fontSize="large" />
          </IconButton>
        </Link>
        <Link to="/posts">
          <IconButton>
            <PeopleIcon fontSize="large" />
          </IconButton>
        </Link>
      </div>
    </div>
  );
}

export default RouteButtons;
